package hu.otpmobil;

import hu.otpmobil.service.WebshopManagerService;

public class WebshopManager {
    public static void main(String[] args) {
        WebshopManagerService webshopManagerService = new WebshopManagerService();
        webshopManagerService.run("customer.csv", "payments.csv");
    }
}