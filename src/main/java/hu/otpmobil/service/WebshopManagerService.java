package hu.otpmobil.service;

import hu.otpmobil.model.Customer;
import hu.otpmobil.model.Payment;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static hu.otpmobil.util.CsvToObjectConverter.convertCsvToObjectListWithValidation;

public class WebshopManagerService {
    private List<Customer> customers;
    private List<Payment> payments;

    public WebshopManagerService() {
        customers = new ArrayList<>();
        payments = new ArrayList<>();
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public void run(String customerFilePath, String paymentFilePath) {
        setCustomers(convertCsvToObjectListWithValidation(Customer.class, customerFilePath));
        setPayments(convertCsvToObjectListWithValidation(Payment.class, paymentFilePath));
        generateReports();
    }

    public void generateReports() {
        try (BufferedWriter customerReportWriter = Files.newBufferedWriter(Path.of("report01.csv"));
             BufferedWriter topCustomersReportWriter = Files.newBufferedWriter(Path.of("top.csv"));
             BufferedWriter webshopReportsWriter = Files.newBufferedWriter(Path.of("report02.csv"))) {

            generateCustomerReport(customerReportWriter);
            generateTopCustomersReport(topCustomersReportWriter);
            generateWebshopReports(webshopReportsWriter);

        } catch (IOException e) {
            logError("Error generating reports: " + e.getMessage());
        }
    }

    public void generateCustomerReport(BufferedWriter reportWriter) {
        try {
            for (Customer customer : customers) {
                int totalAmount = payments.stream()
                        .filter(payment -> payment.getCustomerId().equals(customer.getCustomerId()))
                        .filter(payment -> payment.getWebshopId().equals(customer.getWebshopId()))
                        .mapToInt(Payment::getAmount)
                        .sum();

                reportWriter.write(customer.getName() + "," + customer.getAddress() + "," + totalAmount);
                reportWriter.newLine();
            }
        } catch (IOException e) {
            logError("Error generating customer report: " + e.getMessage());
        }
    }

    public void generateWebshopReports(BufferedWriter reportWriter) {
        try {
            Map<String, Integer> webshopCardPayments = getPaymentsByMethod("card");
            Map<String, Integer> webshopTransferPayments = getPaymentsByMethod("transfer");

            for (String webshopId : webshopCardPayments.keySet()) {
                int cardPayments = webshopCardPayments.getOrDefault(webshopId, 0);
                int transferPayments = webshopTransferPayments.getOrDefault(webshopId, 0);
                reportWriter.write(webshopId + "," + cardPayments + "," + transferPayments);
                reportWriter.newLine();
            }
        } catch (IOException e) {
            logError("Error generating webshop reports: " + e.getMessage());
        }
    }

    public void generateTopCustomersReport(BufferedWriter reportWriter) throws IOException {
        try {
            Map<String, Map<String, Integer>> customerPaymentsByWebshop = payments.stream()
                    .collect(Collectors.groupingBy(Payment::getWebshopId,
                            Collectors.groupingBy(Payment::getCustomerId,
                                    Collectors.summingInt(Payment::getAmount))));

            for (String webshopId : customerPaymentsByWebshop.keySet()) {
                Map<String, Integer> customerPayments = customerPaymentsByWebshop.get(webshopId);

                List<String> topCustomers = customerPayments.entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                        .limit(2)
                        .map(Map.Entry::getKey)
                        .toList();

                List<Customer> webshopCustomers = customers.stream()
                        .filter(customer -> topCustomers.contains(customer.getCustomerId()))
                        .filter(customer -> customer.getWebshopId().equals(webshopId))
                        .limit(2)
                        .toList();

                if (webshopCustomers.size() > 1) {
                    for (int i = 0; i < 2; i++) {
                        Customer customer = webshopCustomers.get(i);
                        String line = String.format("%s,%s,%d", customer.getName(), customer.getAddress(), customerPayments.getOrDefault(customer.getCustomerId(), 0));
                        reportWriter.write(line);
                        reportWriter.newLine();
                    }
                }
            }
        } catch (IOException e) {
            logError("Error generating top customers report: " + e.getMessage());
        }
    }

    private Map<String, Integer> getPaymentsByMethod(String transfer) {
        return payments.stream()
                .filter(payment -> payment.getPaymentMethod().equals(transfer))
                .collect(Collectors.groupingBy(Payment::getWebshopId, Collectors.summingInt(Payment::getAmount)));
    }

    private int calculateTotalAmount(Customer customer) {
        return payments.stream()
                .filter(payment -> payment.getCustomerId().equals(customer.getCustomerId()))
                .mapToInt(Payment::getAmount)
                .sum();
    }

    private void logError(String errorMessage) {
        System.out.println(errorMessage);
    }
}