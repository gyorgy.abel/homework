package hu.otpmobil.model;

import com.opencsv.bean.CsvBindByPosition;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Customer {
    @CsvBindByPosition(position = 0)
    private String webshopId;
    @CsvBindByPosition(position = 1)
    private String customerId;
    @CsvBindByPosition(position = 2)
    @Size(min = 11)
    private String name;
    @CsvBindByPosition(position = 3)
    private String address;
}
