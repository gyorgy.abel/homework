package hu.otpmobil.model;

import com.opencsv.bean.CsvBindByPosition;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Payment {
    @CsvBindByPosition(position = 0)
    private String webshopId;
    @CsvBindByPosition(position = 1)
    private String customerId;
    @CsvBindByPosition(position = 2)
    private String paymentMethod;
    @Min(value = 500)
    @CsvBindByPosition(position = 3)
    private Integer amount;
    @CsvBindByPosition(position = 4)
    private String bankAccount;
    @CsvBindByPosition(position = 5)
    private String cardNumber;
    @CsvBindByPosition(position = 6)
    private String paymentDate;
}
