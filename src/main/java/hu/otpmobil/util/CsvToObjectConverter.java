package hu.otpmobil.util;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import hu.otpmobil.exception.CsvParsingException;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.hibernate.validator.messageinterpolation.ParameterMessageInterpolator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class CsvToObjectConverter {
    private static final String LOG_FILE = "application.log";
    private static final Logger logger = Logger.getLogger(CsvToObjectConverter.class.getName());
    private static final ValidatorFactory validatorFactory = Validation.byDefaultProvider()
            .configure()
            .messageInterpolator(new ParameterMessageInterpolator())
            .buildValidatorFactory();
    private static final Validator validator = validatorFactory.getValidator();

    public static <T> List<T> convertCsvToObjectListWithValidation(Class<T> clazz, String filePath) {
        if (!filePath.isEmpty()) {
            try (InputStream inputStream = CsvToObjectConverter.class.getClassLoader().getResourceAsStream(filePath);
                 BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                CsvToBean<T> csvToBean = new CsvToBeanBuilder<T>(reader)
                        .withType(clazz)
                        .withSeparator(';')
                        .withIgnoreLeadingWhiteSpace(true)
                        .build();

                List<T> objects = csvToBean.parse();

                return objects.stream()
                        .map(CsvToObjectConverter::isValidObject)
                        .filter(clazz::isInstance)
                        .map(clazz::cast)
                        .collect(Collectors.toList());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            throw new CsvParsingException("Please select CSV file!");
        }
        return null;
    }

    private static <T> Object isValidObject(T obj) {
        Set<ConstraintViolation<T>> violations = validator.validate(obj);
        if (violations.isEmpty()) {
            return obj;
        } else {
            try {
                String logFilePath = CsvToObjectConverter.class.getClassLoader().getResource(LOG_FILE).getFile();
                FileHandler fileHandler = new FileHandler(logFilePath, true);
                logger.addHandler(fileHandler);
                logger.log(Level.WARNING, "Skipped the following line: %s".formatted(obj.toString()));
                fileHandler.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
