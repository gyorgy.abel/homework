package hu.otpmobil.util;

import hu.otpmobil.exception.CsvParsingException;
import hu.otpmobil.model.Customer;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CsvToObjectConverterTest {

    @Test
    public void convertCsvToObjectListWithValidation_csvFilesNotExists_noParseToObject() {
        CsvParsingException result =
                assertThrows(CsvParsingException.class, () ->
                        CsvToObjectConverter.convertCsvToObjectListWithValidation(Customer.class, "")
                );

        assertEquals("Please select CSV file!", result.getMessage());
    }

    @Test
    public void convertCsvToObjectListWithValidation_validCsvFilesExist_parseToObject() {
        String filePath = "valid.csv";
        List<Customer> result = CsvToObjectConverter.convertCsvToObjectListWithValidation(Customer.class, filePath);
        Customer expectedUser = new Customer("WP1", "customer1", "Test Elek Janos", "Address1");

        assertEquals(result.get(0), expectedUser);
    }
}